/* 
 * File:   main.cpp
 * Author: joeels
 *
 * Created on 30 November 2016, 9:44 AM
 */

#include "nutzen/http_server/server.hpp"
#include "nutzen/http_server/document_root_request_handler.hpp"
#include "nutzen/http_server/threaded_request_handler.hpp"

#include "nutzen/utilities/logger.hpp"

#include <iostream>
#include <string>

#include <boost/asio.hpp>
#include <boost/thread.hpp>

int main(int _argc, char** _argv) {

    const int argc = 4;
    char* argv[] = {(char*) "", (char*) "/usr/local/var/www/htdocs/", (char*) "0.0.0.0", (char*) "8080"};

    try {
        nutzen::utilities::logger::initialise("nutzen_test");

        typedef nutzen::http::server::document_root_request_handler document_root_request_handler_type;
        typedef nutzen::http::server::request_handler request_handler_type;
        typedef nutzen::http::server::threaded_request_handler threaded_request_handler_type;
        typedef nutzen::http::server::server server_type;

        // Check command line arguments.
        if (argc != 4) {
            std::cerr << "Usage: http_server <doc_root> <address> <port>" << std::endl;
            std::cerr << "  For IPv4, try:" << std::endl;
            std::cerr << "    receiver . 0.0.0.0 80" << std::endl;
            std::cerr << "  For IPv6, try:" << std::endl;
            std::cerr << "    receiver . 0::0 80" << std::endl;

            return 1;
        }

        const request_handler_type::shared_pointer request_handler = threaded_request_handler_type::create(document_root_request_handler_type::create(
                (1 < _argc) ? _argv[1] : argv[1]));
        threaded_request_handler_type::down_cast(request_handler)->start();

        // Initialise the server.
        const server_type::shared_pointer server = server_type::create(
                (2 < _argc) ? _argv[2] : argv[2],
                (3 < _argc) ? _argv[3] : argv[3],
                request_handler);

        // Run the server until stopped.
        (*server)();

    } catch (std::exception& e) {
        std::cerr << "exception: " << e.what() << "\n";
    }

    return 0;
}
