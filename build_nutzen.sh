#!/bin/bash

PREFIX=$PWD

rm -rf include/*
rm -rf lib/*

git submodule update --init --recursive --force

pushd modules/nutzen/ > /dev/null

git checkout -f master

git pull origin master

./automake_add_missing.sh

./configure --prefix=$PREFIX && make && make install

popd > /dev/null
